import numpy
import pytest

from mythen import convert


@pytest.mark.parametrize(
    "nbits, data",
    [
        (
            24,
            [
                0x77000000,
                0x77000001,
                0x77000002,
                0x77000003,
                0x77000004,
                0x77000005,
                0x77000006,
                0x77000007,
            ],
        ),
        (16, [0x00010000, 0x00030002, 0x00050004, 0x00070006]),
        (8, [0x03020100, 0x07060504]),
        (4, [0x76543210]),
    ],
)
def test_convert(nbits, data):
    raw_data = numpy.array(data, dtype="int32")
    array = convert(raw_data, nbits)
    assert array.shape == (8,)
    assert list(array) == list(range(8))


@pytest.mark.parametrize(
    "nbits, data",
    [
        (
            24,
            [
                0xaa000000,
                0xaaffffff,
                0xaafffffe,
                0xaafffffd,
                0xaafffffc,
                0xaafffffb,
                0xaafffffa,
                0xaafffff9,
            ],
        ),
        (16, [0xffff0000, 0xfffdfffe, 0xfffbfffc, 0xfff9fffa]),
        (8, [0xfdfeff00, 0xf9fafbfc]),
        (4, [0x9abcdef0]),
    ],
)
def test_convert_negative(nbits, data):
    raw_data = numpy.array(data, dtype="int32")
    array = convert(raw_data, nbits)
    assert array.shape == (8,)
    assert list(array) == list(range(0, -8, -1))
