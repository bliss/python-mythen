from mythen import Mythen


def main():
    # Instanciate and connect
    mythen = Mythen("mythenid101")

    # Configure acquisition
    mythen.set_delayafterframe(1.)
    mythen.set_exposure_time(1.)
    mythen.set_nframes(5)

    # Start acquisition
    mythen.start()
    print(mythen.get_status())

    # Wait for the 5 frames
    frames = [mythen.readout(1) for _ in range(5)]

    # Stop acquisition
    mythen.stop()
    print(mythen.get_status())

    # Print the frames
    for i, frame in enumerate(frames):
        print("Frame {}: {}".format(i, frame))


if __name__ == "__main__":
    main()
