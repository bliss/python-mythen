Python library for the controlling the mythen MCA
=================================================


Compatible versions
-------------------

All the non-deprecated commands have been implemented from socket interface version `3.0.0` to `4.1.0`.


Hardware testing
----------------
Run the hardware tests with the following command:

``` python
pytest -v --mythen mythenid101
```

Note: mythenid101 must be run the socket interface version `3.0.0`.


Requirements
------------

The library runs with both python 2 and 3:

 - python >= 2.7
 - six
 - numpy
 - enum34 (python < 3.4)


Usage
-----

Example:

``` python
# Imports
from mythen import Mythen

# Instanciate and connect
mythen = Mythen('mythenid101')

# Configure acquisition
mythen.set_delayafterframe(1.)
mythen.set_exposure_time(1.)
mythen.set_nframes(5)

# Start acquisition
mythen.start()
print(mythen.get_status())

# Wait for the 5 frames
frames = [mythen.readout(1) for _ in range(5)]

# Stop acquisition
mythen.stop()
print(mythen.get_status())

# Print the frames
for i, frame in enumerate(frames):
    print('Frame {}: {}'.format(i, frame))
```
